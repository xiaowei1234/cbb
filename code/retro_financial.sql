select substring(cast(week_ending_dt as char), 6) as dt, sum(case when fpd = 1 then apps else 0 end) as fpd_count
    , sum(case when fpd = 1 then apps else 0 end)/sum(apps) as fpd
    , sum(fpd * approval_amount)/sum(approval_amount) as approval_fpd
    , sum(fpd * originations) / sum(originations) as origination_fpd
    , sum(approval_amount) as approval_amount, sum(originations) as originations
    , sum(apps) as apps
from (
    select date_sub(date_add(loan_application_created_date, interval 6 day) , interval dayofweek(loan_application_created_date) day) as week_ending_dt
        , case when first_inst_amount_60dpd - first_inst_amount_paid_60dpd > 1  or chargeback_amount > 0 then 1 else 0 end as fpd
        , sum(approval_amount) as approval_amount
        , sum(outgoing_payment_amount) as originations
        , count(*) as apps
    from lease_summaries
    where merchant_id_transaction = 3 and user_id in (select r_user_id from mongo_handsets where de_need_bank_verification = 1)
        and loan_application_created_date between cast('2018-02-05' as date) and cast('2018-07-05' as date)
        and first_inst_amount_60dpd > 0
    group by 1, 2
    ) as sub
group by 1
order by 1
;