import os

mysql_begin_date = '2017-04-01'
mysql_check_date = '2018-03-09'

# mongo data
# ssh -L 9998:10.128.1.43:27017 xwei@10.128.1.27
# db = client.billfloat

the_filter = {
    "$and": [{"third_party.clarity_cbb":{"$exists": True}}
             , {"decision_engine.decision": True}]
}

# mysql data
# ssh -L 9999:dbreplica.us-east-1.int.smartpaylease.com:3306 xwei@10.128.1.27
mysql_conn_dict = {'user': "risk", 'host': '127.0.0.1', 'port': 9999, 'db': 'billfloat_production', 'passwd': os.environ['mysqlpw']}

# overall path and final merged file name
path = '/Users/xiaowei/repos/CBB_V1/data/'
path2 = '/Users/xiaowei/repos/CBB_V1/data/exp_data/'
mongo_id = 'request.order.application_number'
final_merged_f = '50k_merged.pkl'
# mongo file names and more
ind_f = '50k_raw.pkl'
ident_f = '50k_ident.pkl'
checkpoint = '50k_checkpoint.pkl'
final_df_file = '50k_mongo_final.pkl'

#model file
mod_ident = '50k_mod_ident.pkl'
mod_fname = '50k_model_df.pkl'

# model post feature selection
mod_fs = 'features_selected.pkl'

#intermediate files
X_pruned_1 = 'X_pruned_1.pkl'

#mysql file
msql_f = 'mysql_for50k.pkl'
msql_id = 'application_number'
msql_check = 'mysql_check.pkl'

#bins
bin_fp = '/Users/xiaowei/repos/CBB_V1/outputs/bins.csv' #2 is temporary

# df for final model
fin_mod_df = 'final_model_df.pkl'

#cleaning parameters
mongo_remove = ['_id', 'product', 'third_party.clarity_cbb.clear-bank-behavior.reason-code-description'
                , 'third_party.clarity_cbb.clear-bank-behavior.cbb-reason-code-description2'
                , 'third_party.clarity_cbb.clear-bank-behavior.cbb-reason-code-description'
                , 'run_time', 'third_party.clarity_cbb.clear-bank-behavior.fis-chex-advisor.debit-bureau-reason-texts'
                , 'third_party.clarity_cbb.clear-bank-behavior.accounts.account.reason-codes'
                , 'extra_variables.merchant_channel_name', 'extra_variables.merchant_channel_number'
                , 'extra_variables.geocode_user_latitutde', 'extra_variables.geocode_user_longitude'
                , 'extra_variables.prev_handsets_decision_score'
                , 'third_party.clarity_cbb.clear-bank-behavior.full-name'
                , 'third_party.clarity_cbb.backend_extra_variables.timestamp'
                , 'request.location_address.state'
                , 'extra_variables.prev_handsets_decision_offer_code'
                , 'extra_variables.geocode_location_longitude'
                # , 'extra_variables.prev_handsets_decision_cart_limit'
                , 'pmml_variables.probability(0)'
                , 'pmml_variables.probability(1)'
                , 'pmml_variables.y'
                , 'pmml_variables.cbb_output_approval_amount'
                , 'pmml_variables.cbb_output_is_approved'
                , 'pmml_variables.cbb_output_score'
                , 'pmml_variables.cbb_prev_handsets_cart_limit'
                # , 'pmml_variables.prev_handsets_decision_cart_limit'
                , 'pmml_variables.cbb_prev_handsets_score'
                , 'extra_variables.is_more_info_flow'
                # , 'request.location.funder_id'
                , 'extra_variables.prev_handsets_decision_is_more_info_bank'
                ]

mongo_big_drop = ['request.', 'third_party.clarity_cbb.inquiry.', 'decision_engine.']
ident_vars = ['request.order.application_number', 'run_time_ms']
contains_vars = ['request.more_info.employer.employer_phone', 'request.user.apt_suite_number'
                , 'third_party.clarity_cbb.clear-bank-behavior.accounts.account.routing-number'
                , 'third_party.clarity_cbb.clear-bank-behavior.fis-chex-advisor.closures.closure.routing-number'
                ]

mongo_keep = ['request.location_address.state', 'request.location_sales_incentive', 'request.merchant.partner_id'
    , 'request.merchant.subgroup', 'request.user.age_in_years', 'request.user.ssn_frozen'
    , 'request.loan_application.fe_store_transactions_28', 'request.loan_application.fe_store_transactions_7'
    , 'request.loan_application.fe_user_has_chargeback', 'request.loan_application.is_cart_boost'
    , 'request.loan_application.is_web_checkout', 'request.location.funder_id', 'request.user.is_admin'
    , 'request.user.is_military', 'request.user.is_suspended'
    , 'request.loan_application.fe_address_summary.distinct_users_w_addr_count'
    , 'request.loan_application.fe_address_summary.hard_fail_count'
    , 'decision_engine.score.points'] + contains_vars + ident_vars

de_score_col = 'decision_engine.score.points'
# when value got combined into list
prefix = 'third_party.clarity_cbb.clear-bank-behavior.accounts'
prefix2 = '.account.'

chex_pre = 'third_party.clarity_cbb.clear-bank-behavior.fis-chex-advisor.closures'
chex_pre2 = '.closure.'

# bureau score
miss_num = '9999'
miss_num_col = 'third_party.clarity_cbb.clear-bank-behavior.fis-chex-advisor.debit-bureau-score'

# pmml nulls
miss_pmml = 'NA'
miss_pmml_cols = ['pmml_variables.fe_payhist_days_difference'
                , 'pmml_variables.fe_payhist_is_user_phone_number_match'
                , 'pmml_variables.fe_retail_velocity_z_score'
                , 'pmml_variables.fe_tracfone_score_code']

# fill with 0s
zero_fills = ['request.loan_application.fe_address_summary.distinct_users_w_addr_count'
               , 'request.loan_application.fe_address_summary.hard_fail_count'
               , 'request.user.is_military'
               ]

# make null values based upon field
make_null_field = 'third_party.clarity_cbb.clear-bank-behavior.check-cashing-history'
null_fields = 'third_party.clarity_cbb.clear-bank-behavior.check-cashing.'

# empty thresh
empty_thresh = 0.03

#number to category
cat_cols = ['pmml_variables.fe_merchant_id', 'request.location.funder_id', 'request.merchant.partner_id'
, 'third_party.clarity_cbb.clear-bank-behavior.fis-chex-advisor.closures.closure.reason-code']

#special handling
date_cols = ['pmml_variables.fe_address_summary_address_first_seen', 'pmml_variables.fe_address_summary_address_last_seen']
address_col = ['pmml_variables.fe_address_summary_normalized_address']

# character fields thresholds
thresh_min = 0.05
max_num = 5

# dummies exclude
d_exclude = ident_vars + [msql_id]

#V4 rollout epoch timestamp
v4_ts = 1494866160000

# imputation strategy
impute_strat = 'most_frequent'


corr_thresh = 0.85


#CBB groups
cbb_grps = [540, 600, 650, 700, 725]


# current cbb thresholds
cbb_score2_thresh = [540, 600, 650, 700]

model_extra_vars = [
# 'pmml_variables.fe_days_since_location_created'
# , 'pmml_variables.fe_age_in_years'
# , 'extra_variables.geocode_location_to_user_distance_miles'
]


# model approval cutoffs
approval_cuts = [0.095, 0.108, 0.12, 0.132, 0.144, 0.15, 0.162, 0.17, 0.18, 0.19, 0.21, 0.23, 0.29, 0.3]

retro_df = 'retro.pkl'

retro_renames = {'pmml_variables.cbb_num_of_ssns'
                    : 'third_party.clarity_cbb.clear-bank-behavior.accounts.account.number-of-ssns'
                , 'pmml_variables.cbb_debit_bureau_score'
                    : 'third_party.clarity_cbb.clear-bank-behavior.fis-chex-advisor.debit-bureau-score'
                , 'pmml_variables.cbb_num_unknown_risk_accounts'
                    : 'third_party.clarity_cbb.clear-bank-behavior.number-of-unknown-risk-accounts'
                , 'pmml_variables.cbb_num_payday_inquiries_60_days_ago'
                    : 'third_party.clarity_cbb.clear-bank-behavior.fis-chex-advisor.number-of-payday-inquiries.sixty-days-ago'
                , 'pmml_variables.user_distance_bucketed'
                    : 'extra_variables.geocode_location_to_user_distance_miles'
                , 'pmml_variables.cbb_num_inquiries_90_days_ago'
                    : 'third_party.clarity_cbb.clear-bank-behavior.fis-chex-advisor.number-of-inquiries.ninety-days-ago'
                , 'pmml_variables.cbb_num_closures_paid_one_year_ago'
                    : 'third_party.clarity_cbb.clear-bank-behavior.fis-chex-advisor.number-closures-paid.one-year-ago'
                , 'pmml_variables.cbb_num_closures_one_year_ago'
                    : 'third_party.clarity_cbb.clear-bank-behavior.fis-chex-advisor.number-closures.one-year-ago'
                , 'pmml_variables.cbb_default_rate_60_days_ago'
                    : 'third_party.clarity_cbb.clear-bank-behavior.accounts.account.default-rate-60-days-ago'
                , 'pmml_variables.cbb_address_state_tx'
                    : 'pmml_variables.fe_loan_application_address_state_TX'
                , 'pmml_variables.user_age_bucketed'
                    : 'pmml_variables.fe_age_in_years'
                , 'pmml_variables.cbb_amount_closures_unpaid_five_years_ago'
                    : 'third_party.clarity_cbb.clear-bank-behavior.fis-chex-advisor.amount-closures-unpaid.five-years-ago'
                , 'pmml_variables.cbb_num_closures_unpaid_three_years_ago'
                    : 'third_party.clarity_cbb.clear-bank-behavior.fis-chex-advisor.number-closures-unpaid.three-years-ago'
                , 'pmml_variables.cbb_amount_closures_30_days_ago'
                    : 'third_party.clarity_cbb.clear-bank-behavior.fis-chex-advisor.amount-closures.thirty-days-ago'
                , 'pmml_variables.cbb_amount_closures_unpaid_two_years_ago'
                    : 'third_party.clarity_cbb.clear-bank-behavior.fis-chex-advisor.amount-closures-unpaid.two-years-ago'
                , 'pmml_variables.prev_handsets_decision_cart_limit'
                    : 'extra_variables.prev_handsets_decision_cart_limit'
                , 'pmml_variables.prev_handsets_decision_score'
                    : 'decision_engine.score.points'
                , 'pmml_variables.cbb_days_since_first_seen_by_clarity'
                    : 'third_party.clarity_cbb.clear-bank-behavior.accounts.account.days-since-first-seen-by-clarity'
                , 'pmml_variables.cbb_account_stability_24_hours_ago'
                    : 'third_party.clarity_cbb.clear-bank-behavior.account-stability.twentyfour-hours-ago'
                , 'pmml_variables.cbb_days_since_location_created'
                    : 'pmml_variables.fe_days_since_location_created'
                , 'pmml_variables.cbb_score_bucketed'
                    : 'third_party.clarity_cbb.clear-bank-behavior.cbb-score'
                , 'pmml_variables.cbb_num_high_risk_factors'
                    : 'third_party.clarity_cbb.clear-bank-behavior.accounts.account.high-risk-factors'
                , 'pmml_variables.cbb_num_non_dda_inquiries_two_years_ago'
                    : 'third_party.clarity_cbb.clear-bank-behavior.fis-chex-advisor.number-of-non-dda-inquiries.two-years-ago'
                , 'v4_score' : 'decision_engine.score.points' 
                    }


retro_renames_no_bucket = {'pmml_variables.cbb_num_of_ssns'
                    : 'third_party.clarity_cbb.clear-bank-behavior.accounts.account.number-of-ssns'
                , 'pmml_variables.cbb_debit_bureau_score'
                    : 'third_party.clarity_cbb.clear-bank-behavior.fis-chex-advisor.debit-bureau-score'
                , 'pmml_variables.cbb_num_unknown_risk_accounts'
                    : 'third_party.clarity_cbb.clear-bank-behavior.number-of-unknown-risk-accounts'
                , 'pmml_variables.cbb_num_payday_inquiries_60_days_ago'
                    : 'third_party.clarity_cbb.clear-bank-behavior.fis-chex-advisor.number-of-payday-inquiries.sixty-days-ago'
                # , 'pmml_variables.user_distance_bucketed'
                #     : 'extra_variables.geocode_location_to_user_distance_miles'
                , 'pmml_variables.cbb_num_inquiries_90_days_ago'
                    : 'third_party.clarity_cbb.clear-bank-behavior.fis-chex-advisor.number-of-inquiries.ninety-days-ago'
                , 'pmml_variables.cbb_num_closures_paid_one_year_ago'
                    : 'third_party.clarity_cbb.clear-bank-behavior.fis-chex-advisor.number-closures-paid.one-year-ago'
                , 'pmml_variables.cbb_num_closures_one_year_ago'
                    : 'third_party.clarity_cbb.clear-bank-behavior.fis-chex-advisor.number-closures.one-year-ago'
                , 'pmml_variables.cbb_default_rate_60_days_ago'
                    : 'third_party.clarity_cbb.clear-bank-behavior.accounts.account.default-rate-60-days-ago'
                , 'pmml_variables.cbb_address_state_tx'
                    : 'pmml_variables.fe_loan_application_address_state_TX'
                # , 'pmml_variables.user_age_bucketed'
                #     : 'pmml_variables.fe_age_in_years'
                , 'pmml_variables.cbb_amount_closures_unpaid_five_years_ago'
                    : 'third_party.clarity_cbb.clear-bank-behavior.fis-chex-advisor.amount-closures-unpaid.five-years-ago'
                , 'pmml_variables.cbb_num_closures_unpaid_three_years_ago'
                    : 'third_party.clarity_cbb.clear-bank-behavior.fis-chex-advisor.number-closures-unpaid.three-years-ago'
                , 'pmml_variables.cbb_amount_closures_30_days_ago'
                    : 'third_party.clarity_cbb.clear-bank-behavior.fis-chex-advisor.amount-closures.thirty-days-ago'
                , 'pmml_variables.cbb_amount_closures_unpaid_two_years_ago'
                    : 'third_party.clarity_cbb.clear-bank-behavior.fis-chex-advisor.amount-closures-unpaid.two-years-ago'
                , 'pmml_variables.prev_handsets_decision_cart_limit'
                    : 'extra_variables.prev_handsets_decision_cart_limit'
                , 'pmml_variables.prev_handsets_decision_score'
                    : 'decision_engine.score.points'
                , 'pmml_variables.cbb_days_since_first_seen_by_clarity'
                    : 'third_party.clarity_cbb.clear-bank-behavior.accounts.account.days-since-first-seen-by-clarity'
                , 'pmml_variables.cbb_account_stability_24_hours_ago'
                    : 'third_party.clarity_cbb.clear-bank-behavior.account-stability.twentyfour-hours-ago'
                , 'pmml_variables.cbb_days_since_location_created'
                    : 'pmml_variables.fe_days_since_location_created'
                # , 'pmml_variables.cbb_score_bucketed'
                #     : 'third_party.clarity_cbb.clear-bank-behavior.cbb-score'
                , 'pmml_variables.cbb_num_high_risk_factors'
                    : 'third_party.clarity_cbb.clear-bank-behavior.accounts.account.high-risk-factors'
                , 'pmml_variables.cbb_num_non_dda_inquiries_two_years_ago'
                    : 'third_party.clarity_cbb.clear-bank-behavior.fis-chex-advisor.number-of-non-dda-inquiries.two-years-ago'
                , 'v4_score' : 'decision_engine.score.points'
                    }

trend_vars = ['pmml_variables.cbb_num_closures_one_year_ago'
            , 'third_party.clarity_cbb.clear-bank-behavior.fis-chex-advisor.number-closures.three-years-ago']

to_logs = [
    'third_party.clarity_cbb.clear-bank-behavior.fis-chex-advisor.number-of-payday-inquiries.number-since-first-inquiry'
    , 'pmml_variables.fe_store_transactions_7'
    , 'third_party.clarity_cbb.clear-bank-behavior.fis-chex-advisor.number-of-non-dda-inquiries.number-since-last-inquiry'
    # , 'third_party.clarity_cbb.clear-bank-behavior.estimated-bank-history'
]