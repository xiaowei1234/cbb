import numpy as np
import pandas as pd
import constants as c
import re
from functools import partial, update_wrapper


def wrapped_partial(func, *args, **kwargs):
    """
    partial function that preserves function name
    """
    partial_func = partial(func, *args, **kwargs)
    update_wrapper(partial_func, func)
    return partial_func


def num_start(astr):
    """
    not used
    returns 1 or 0 whether astr starts with a # or not
    """
    if isinstance(astr, (int, float)):
        return 1
    if not isinstance(astr, str):
        return 0
    match = bool(re.match(r'^[0-9]+', astr))
    return np.int8(match)


is_iter = lambda thing: hasattr(thing, '__iter__') and not isinstance(thing, str)

make_list = lambda thing: thing if is_iter(thing) else [thing]

has_one_ele = lambda v: hasattr(v, '__iter__') and len(v) > 0

is_float_binary = lambda v: np.abs(v -1) < .00001 or abs(v) < .00001 

# detect and convert certain data types
bool_comp = lambda v: type(v) == bool or (isinstance(v, str) and v.lower() in ('true', 'false'))

bool_conv = lambda v: 1 if v == True or (isinstance(v, str) and v.lower() == 'true') else 0

float_comp = lambda chr: bool(re.match(r"^[0-9]*\.[0-9]+$", chr)) if isinstance(chr, str) else False

float_conv = lambda chr: np.float64(chr) if float_comp(chr) or int_comp(chr) else np.nan

int_comp = lambda chr: bool(re.match("^[0-9]+$", chr)) if isinstance(chr, str) else False

int_conv = lambda chr: np.int64(chr) if int_comp(chr) else np.nan

iter_comp = is_iter


def iter_conv(itr, delim='|'):
    if iter_comp(itr):
        return delim.join(itr)
    return itr


def comp_char(astr, delim):
    if not isinstance(astr, str):
        return False
    strp = astr.strip()
    idx = strp.find(delim)
    return idx > 0 and len(strp) > idx + 1


bar_comp = wrapped_partial(comp_char, delim='|')


def conv_delim(astr, delim):
    """
    """
    if not isinstance(astr, str):
        return []
    re_func = lambda s: re.sub(' ', '_', re.sub(r'[\W ]+', '', s)).strip()
    return [s for s in map(re_func, astr.split(delim)) if len(s) > 0]


bar_conv = wrapped_partial(conv_delim, delim='|')

# detect for preprocessing.py

def zero_comp(series):
    v_cnts = series.value_counts()
    non_valid = [v for v in v_cnts.index if not is_float_binary(v)]
    return len(non_valid) == 0


fill_zeros = lambda series: pd.series.fillna(0)

# end detect and convert

def has_something(v):
    if pd.isnull(v):
        return False
    if has_one_ele(v):
        return True
    if isinstance(v, str):
        return len(v.strip()) > 0
    if isinstance(v, (int, float)):
        return pd.notnull(v)



def detect(df, comp_func, thresh, detect_rows, remove=c.cat_cols):
    df_selected = df.select_dtypes(include=['object'])
    middle = int(df.shape[0]/2)
    has_cols = []
    columns = [co for co in df_selected.columns if co not in remove]
    for col in columns:
        conc = pd.concat([df[col][:detect_rows], df[col][middle:middle + detect_rows], df[col][-detect_rows:]])
        fl100 = [v for v in conc if comp_func(v)]
        if len(fl100) >= thresh:
            has_cols.append(col)
    return has_cols


def explode_to_df(val):
    """
    df of a single row of ones with index = val
    """
    return pd.Series(np.repeat(1, len(val)), index=val, dtype=np.float64).to_frame().T


def series_to_df(series):
    """
    """
    lst = series.apply(explode_to_df)
    assert lst.size == series.size
    return pd.concat(list(lst))


def join_large_dfs(lst):
    """
    for some reason pd.concat with axis=1 and large dataframes results in hanging
        this implements concat using numpy (which is the backend of pd.concat)
    """
    names = []
    dtypes = []
    for df in lst:
        nd = df.dtypes
        nm = list(nd.index.values)
        dty = list(nd.values)
        names.extend(nm)
        dtypes.extend(dty)
    mat_lst = tuple([df.as_matrix() for df in lst])
    total = np.concatenate(mat_lst, axis=1)
    df = pd.DataFrame(total, columns=names)
    for col, dty in zip(df.columns, dtypes):
        df[col] = df[col].astype(dty)
    return df


def make_other(v, keeps):
    if not has_something(v):
        return np.nan
    if v not in keeps:
        return np.nan # changed because 'other' is too hard to code in production
    return v


def char_column_revise(series, min_num, max_num=c.max_num):
    """
    for series of strings for the top max_num values by count with at least min_num counts
        keep the same otherwise set to 'Other'
    """
    series = series.apply(lambda v: v if isinstance(v, str) else np.nan)
    cnts = series.value_counts()[:max_num]
    keep_vals = cnts[cnts > min_num].index.values
    if keep_vals.size == 0:
        return pd.Series(np.repeat(np.nan, series.size))
    series = series.apply(make_other, args=(keep_vals,))
    return series
