select application_number as app_code
    , case when (chargeback_amount > 0 or first_inst_amount_30dpd - first_inst_amount_paid_30dpd > 1
           )
             and early_payment_status is null
        then 1 else 0 end as y, outgoing_payment_amount
from lease_summaries
where first_inst_amount_30dpd > 0 and outgoing_payment_amount > 200 and loan_application_created_date > '{bd}'
and merchant_id_transaction = 3
;