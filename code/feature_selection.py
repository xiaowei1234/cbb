import numpy as np
import pandas as pd
import constants as c
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.metrics import roc_auc_score
from sklearn.metrics import log_loss
from sklearn.feature_selection import RFE
from cleaning import separate_df_bycols
from sklearn.metrics import make_scorer
import helpers


def split_latest(*args):
    """
    given args of dataframes or series or numpy arrays split along the first dimension into 90/10 and return
    """
    things = []
    for thing in args:
        cut = np.int64(thing.shape[0]/10)
        things.append(thing[:-cut])
        things.append(thing[-cut:])
    return things


def get_x_y(df):
    """
    returns X and y from df
    """
    y, x = separate_df_bycols(df, cols='y')
    return x, np.ravel(y['y'])


def mod_auc(y, yhat, cut=0.5):
    """
    modified auc going from 0 to 1 and only probs < smaller
    """
    smaller = yhat < cut
    return (roc_auc_score(y[smaller], yhat[smaller]) -0.5) * 2


def proba_scorer(y_true, y_pred, func):
    """
    probability scorer wrapper
    """
    return func(y_true, y_pred[:, 1])
    # return func(y_true, y_pred)


mod_auc_scorer = make_scorer(proba_scorer, needs_proba=True, func=mod_auc)

auc_scorer = make_scorer(proba_scorer, needs_proba=True, func=roc_auc_score)

log_scorer = make_scorer(proba_scorer, needs_proba=True, func=log_loss, greater_is_better=False)


def eval_model(mod, X, y, metric=mod_auc, fit_mod=True):
    """
    fits model on train vs test and returns the model, train, and test metrics
    """
    X_train, X_test, y_train, y_test = split_latest(X, y)
    if fit_mod:
        mod.fit(X_train, y_train)
    pred_train = mod.predict_proba(X_train)[:, 1]
    pred_test = mod.predict_proba(X_test)[:, 1]
    return mod, metric(y_train, pred_train), metric(y_test, pred_test)


def get_importances(X, y, alg=GradientBoostingClassifier, metric=mod_auc, **kwargs):
    """
    get importances or coefficient values from alg
    """
    mod = alg(**kwargs)
    model, train_score, score = eval_model(mod, X, y, metric)
    if hasattr(model, 'feature_importances_'):
        importance = model.feature_importances_
    else:
        importance = model.coef_
    return dict(zip(X.columns, importance)), train_score, score, model


def make_corr_list(df, corr_thresh=c.corr_thresh):
    """
    for correlation matrix if correlation between two columns are greater than corr_thresh then append tuple to returned list
    """
    corrs = df.corr()
    complist = []
    for idx, cols in enumerate(corrs):
        for i in range(idx+1, corrs.shape[0]):
            if corrs.iloc[i, idx] > corr_thresh:
                complist.append((corrs.index.values[i], corrs.columns[idx]))
    return complist


def kick_correlated(importance_dict, complist):
    """
    for tuples of correlated features (complist) add to returned set the one that has lower importance (importance_dict)
    """
    # to_del = [col for col in filter(lambda k: importance_dict[k] < 0.00001, importance_dict)]
    to_del = []
    for comp in complist:
        if importance_dict[comp[0]] < importance_dict[comp[1]]:
            to_del.append(comp[0])
        else:
            to_del.append(comp[1])
    return set(to_del)


def run_model(params, alg, X, y, param_interest, metric=mod_auc):
    """
    run model and return values associated with it
    """
    if not helpers.is_iter(param_interest):
        param_interest = [param_interest]
    saves = {p: params[p] for p in param_interest}
    mod = alg(**params)
    saves['model'] , saves['train metric'], saves['metric']  = eval_model(mod, X, y, metric)
    return saves


def mods_to_df_C(models, param='C'):
    """
    for models produced by run_model output model values to a dataframe
    """
    if helpers.is_iter(param):
        s_lst = []
        for p in param:
            s_lst.append(pd.Series([mod[p] for mod in models], name=p))
        params = pd.concat(s_lst, axis=1)
    else:
        params = pd.Series([mod[param] for mod in models], name='reg_param')
    coefficients = pd.Series([np.sum(abs(mod['model'].coef_) > 0.00001) for mod in models], name='coefficients')
    train = pd.Series([(mod['train metric']) for mod in models], name='train metric')
    metric = pd.Series([(mod['metric']) for mod in models], name='metric')
    return pd.concat([params, coefficients, train, metric], axis=1)


def select_log_columns(matrix, cols=c.to_logs):
    return matrix.loc[:, cols]


def make_log(array):
    return np.log(array + 1)
