# Clear Bank Behavior Fraud/Risk Model

## Introduction and Summary

### Background

This repository is everything relating to the model to determine risk when an applicant heads down the 'more info' path and inputs their banking information. The model incorporates the main model's decision score as well as other features (that may be found in the main model) and also adds in features from Clarity Bank Behavior.

### Requirements and Precautions

Because this is a 'secondary' model that was trained on applicants that were purposefully diverted to this 'more info' path, this model should not be used as a standalone model. Also, it is inadvisable to dramatically loosen the criteria of applicants heading down the 'more info' path as the model was trained only on a subset of applicants that the main model deemed 'high risk' but not 'too high risk'. Please see [Google Doc](https://docs.google.com/spreadsheets/d/1LwR3Ef-umO9VPH9vgosynwA6hwG1CCK_18A9rRAo-Lk/edit?usp=sharing).

If the main model is retrained it is advised that this model be retrained at the same time using the new main model's decision.score input as the main model's decision.score is an important feature in this model

## Navigating this Repository

### Notebooks folder

*Mongo_Connection.ipynb* - connect to mongodb and get data

*MySQL_Connection.ipynb* - connect to MySQL and get data

*Mongo_Clean.ipynb* - cleaning mongo data

*Merge_Fill.ipynb* - merge MySQL and mongo data as well as create dummies

*Feature_Selection.ipynb* - feature selection

*WoE_investigation.ipynb* - feature binning

*Make_Final_DF.ipynb* - merge in bins and make final dataframe

*Modeling.ipynb* - final model and pipeline

*PMML_Pipeline.ipynb* - make pipeline and pmml file

*Examining_Model_Features.ipynb* - quick look into features used in final model

*side folder* - side analysis that isn't part of the main flow


### Code folder

*cleaning.py* - functions for cleaning up the raw data

*preprocessing.py* - preprocessing data such as imputation and standardization

*feature_selection.py* - feature selection code and model evaluation

*PyWoE.py* - Python Weight of Evidence code (from the internet)

*evaluate.py* - for evaluating and comparing models

*helpers.py* - small functions (mostly called from cleaning.py)

*constants.py* - constants so that there is a central location for parameters

*mysql_pull.sql* - sql code for mysql data

### Outputs folder

*CBB_model.pmml* - pmml file

*model_dictionary.xlsx* - data dictionary and info on how to create features needed in pmml file

## Order of Operations

1. Mongo_Connection and MySQL_Connection to get the raw data

2. Mongo_Clean to clean mongo data

3. Merge_Fill to merge the two tables and create dummies

4. Feature_Selection to select features

5. WoE_investigation to determine which features and bins

6. Make_Final_DF to make final dataframe for modeling

7. Modeling to determine hyperparameters and other pipeline options

8. PMML_Pipeline creates final model dump and pmml file

9. Examining_Model_Features outputs a csv for model_dictionary.xlsx

10. Model_Validation does model validation

11. Calculate_Cutoffs calculates new cutoffs

## Next model iteration

1. Evaluate model performance

2. Experiment with adding in routing number (as a binned variable)

3. Experiment with binning reason codes

4. Refresh model with new data

5. Experiment with tuning regularization parameter

6. Fine tune the character fields to dummy process

7. Experiment with binning day of week or day of month as a variable

