import pandas as pd
import numpy as np
import constants as c
import os


def make_pct(series):
    summed = series.sum()
    return summed / len(series)


def make_group(val, groupings):
    """
    """
    length = len(groupings)
    suffix = ''
    i = 0
    while i < length:
        if val < groupings[i]:
            suffix =  '<' + str(groupings[i])
            break
        i += 1
    if i >= 1:
        return '>' + str(groupings[i-1]) + ' & ' + suffix
    return suffix


def grps_to_df(lists, headers=['Group', 'Defaults', 'Count', 'Pred Ratio']):
    """
    """
    df = pd.DataFrame(lists, columns=headers)
    df['Ratio'] = df.Defaults / df.Count
    return df


def perc_grps(series, splits=0.1, aesc=True):
    """
    """
    series2 = series.sort_index(ascending=aesc)
    chunk = np.int64(series2.size * splits)
    start = 0
    rows = []
    ratio = 0
    while start < series2.size -1:
        name = 'Top ' + str(int(ratio * 100)) + r'% - ' + str(int((ratio + splits) * 100)) + r'%'
        defaults = np.sum(series2.iloc[start:start + chunk])
        cnt = min(start + chunk, series2.size) - start
        # pred_ratio = np.mean(series2.index.values[start:start + chunk])
        pred_ratio = np.mean(series2.index.get_level_values(-1)[start:start + chunk])
        rows.append([name, defaults, cnt, pred_ratio])
        ratio += splits
        start += chunk
    return grps_to_df(rows)


def ratio_numbers(array, cut_num):
    return np.sum(array >= cut_num)


def sort_desc(array):
    return -np.sort(-array)


def array_col(array, grps, array2):
    """
    for each cutoff in grps that exist in array find the corresponding cut in array2

    """
    to_have = np.array([ratio_numbers(array, cut) for cut in grps])
    not_zeros = to_have > 0
    array2_sorted = sort_desc(array2)
    return np.array(grps)[not_zeros], [array2_sorted[idx - 1] for idx in to_have[not_zeros]]


def engine_differences(orig_scores, new_scores, cutoff=0.18):
    rows = []
    for i in range(1, np.int8(cutoff*100+1)):
        pct = i/100.
        orig = np.sum(orig_scores < pct)
        new = np.sum(new_scores < pct)
        rows.append([pct, orig, new])
    return pd.DataFrame(rows, columns=['Cutoff', 'V4', 'New']).set_index('Cutoff')


def engine_shifts(orig_scores, new_scores, window=20, cutoff=None):
    """
    """
    df = pd.DataFrame({'orig': orig_scores, 'new': new_scores})
    df['difference'] = pd.Series(new_scores - orig_scores)
    if cutoff is not None:
        df = df.loc[df.orig < cutoff, :]
    df = df.set_index('orig').sort_index()
    df['smoothed'] = df.difference.rolling(window=window, center=True).mean()
    return df


def float_round(x, base=.05):
    return round(base * round(x/base), 2)


def pdf_to_excel(output_path, sheetnames, pdfs, name='clustering.xlsx'):
    if isinstance(sheetnames, str):
        sheetnames = [sheetnames]
        pdfs = [pdfs]
    if not os.path.exists(output_path):
        os.makedirs(output_path)
    fp = output_path + '/' + name
    writer = pd.ExcelWriter(fp, engine='xlsxwriter')
    for tab, pdf in zip(sheetnames, pdfs):
        pdf.to_excel(writer, tab)
    writer.save()
    writer.close()
