import numpy as np
import pandas as pd
import constants as c
import helpers


def manual_remove(df, mongo_keep=c.mongo_keep, mongo_remove=c.mongo_remove, mongo_big_drop=c.mongo_big_drop):
    """
    drops columns from df that start with mongo_big_drop or equals monvo_remove but not in mongo_keep
    """
    all_cs = [cols for cols in df.columns if cols not in mongo_keep]
    drops = mongo_remove
    for col in all_cs:
        for d in mongo_big_drop:
            if col.find(d) == 0:
                drops.append(col)
                break
    # print (drops)
    drops = [d for d in set(drops) if d in df.columns]
    return df.drop(drops, axis=1)



def fix_cbb_filter(df, column=c.prefix):
    """
    returns columns that start with column and rows where the value is an iterable. 
        Used for unpacking lists that should have been a dictionary
    """
    fills = filter(lambda col: col.find(column) == 0, df.columns)
    has_iter = df[column].apply(helpers.has_one_ele)
    return df.loc[has_iter, fills]


def fix_cbb(series, prefix=c.prefix, prefix2=c.prefix2, ele=0):
    """
    given a row of values (series) that start with prefix find values within iterable named prefix and set
        values within prefx + prefix2 + key within the aforementioned iterable with name prefix
    """
    lst = series[prefix]
    prefix_plus = prefix + prefix2
    for key in lst[ele]: # use the last one
        if prefix_plus + key in series:
            series[prefix_plus + key] = lst[ele][key]
    series[prefix] = ''
    return series


def fill_zeros_df(df, cols=c.zero_fills):
    """
    fill null values within columns within df denoted by cols with 
    """
    df[cols] = df[cols].fillna(0)
    return df


def find_not_empty(series, empty_thresh=c.empty_thresh):
    """
    return columns that have at least empty_thresh ratio of non null or empty values
    """
    not_null = pd.notnull(series)
    not_empty = series.apply(lambda v: not hasattr(v, '__iter__') or len(v) > 0)
    return (sum(not_null & not_empty) / series.size) > empty_thresh or 'pmml_variables.cbb' in series.name


def make_contains_var(df, columns=c.contains_vars):
    """
    transforms columns into binary variable for 1 if column is not null/empty and 0 otherwise
    """
    for col in columns:
        df[col] = df[col].apply(lambda v: 1 if helpers.has_something(v) else 0)
    return df


def address_col(df, col=c.address_col):
    """
    currently not used
    """
    df[col] = df[col].apply(helpers.num_start).astype(np.int8)
    return df


def make_null_fields(df, col=c.miss_num_col, miss_num=c.miss_num):
    """
    fix credit_bureau_score field (but can be modified  to use for other fields)
    for col in df that equals miss_num set to null. Add column no_bureau_score to denote missing
    """
    for onecol in helpers.make_list(col):
        df[onecol] = df[onecol].apply(lambda v: v if v != miss_num else np.nan)
    return df


def make_missing_based_upon(df, start_str=c.null_fields, foi=c.make_null_field, missval='false'):
    """
    where foi values are equal to miss_val set all columns that start with start_str equal to null
    """
    make_null_rows = df[foi] == missval
    null_cols = [col for col in df.columns if col.find(start_str) >= 0]
    df.loc[make_null_rows, null_cols] = np.nan
    return df


def separate_df_bycols(df, cols=c.ident_vars):
    """
    split a dataframe into two with the first df containing cols and the second every other column
    """
    one = df[helpers.make_list(cols)]
    two = df.drop(cols, axis=1)
    return one, two


def detect_convert_lst(df, comp_func, thresh=1, detect_rows=100):
    """
    given comp_func that compares values and returns true if criteria met, split out values that are delimited by '.'
        this is used for breaking out the reason codes
    """
    detect_cols = helpers.detect(df, comp_func, thresh, detect_rows)
    # print ('columns to extract: ', detect_cols)
    dfs_gen = map(lambda col: helpers.series_to_df(df[col]), detect_cols)
    dfs_lst = [df.rename(columns=lambda oname: '.'.join(cname.split('.')[-2:]) + '.' + oname).fillna(0) for cname, df in zip(detect_cols, dfs_gen)]
    # print ('dataframes created')
    df2 = df.drop(detect_cols, axis=1)
    return helpers.join_large_dfs([df2] + dfs_lst)


def detect_convert_lst2(df, comp_func, thresh=1, detect_rows=100):
    """
    not used. Was originally used to debug detect_convert_lst
    """
    detect_cols = helpers.detect(df, comp_func, thresh, detect_rows)
    print ('columns to extract: ', detect_cols)
    dfs_gen = map(lambda col: helpers.series_to_df(df[col]), detect_cols)
    dfs_lst = []
    for name, adf in zip(detect_cols, dfs_gen):
        t_df = adf.rename(columns=lambda oname: name.split('.')[-1] + '.' + oname)
        dfs_lst.append(t_df)
        print (name, t_df.shape)
    print ('dataframes created')
    df2 = df.drop(detect_cols, axis=1)
    print ('orig cols dropped')
    return df2, pd.concat(dfs_lst, axis=1, ignore_index=True)


def detect_convert_type(df, comp_func, conv_func, thresh=1, ctyp=np.int8, detect_rows=100):
    """
    generic function wrapped by other functions used to 
    """
    # print ('function: ' + comp_func.__name__)
    detect_cols = helpers.detect(df, comp_func, thresh, detect_rows)
    # print ('columns detected: ' + '\n'.join(detect_cols))
    for col in detect_cols:
            df[col] = df[col].apply(conv_func)
            if ctyp is not None:
                df[col] = df[col].astype(ctyp)
    return df


def convert_bool(df):
    """
    convert boolean columns in df to binary
    """
    df_bool = df.select_dtypes(include=['bool'])
    for col in df_bool.columns:
        df[col] = df[col].apply(helpers.bool_conv).astype(np.int8)
    return df


def filter_char_columns(df, thresh_min=c.thresh_min, make_cat=c.cat_cols):
    """
    for string columns with many values this function only selects the
        top X number of values and buckets other values into 'Other'
    """
    num = df.shape[0]
    min_num = num * thresh_min
    for col in make_cat:
        df[col] = df[col].apply(lambda v: str(v) if helpers.has_something(v) and isinstance(v, (str, int, float, bool)) else np.nan)
    char_df = df.select_dtypes(include=['object'])
    cols = list(char_df.columns)
    df[cols] = df[cols].apply(helpers.char_column_revise, args=(min_num,))
    non_null_cnts = df[cols].apply(lambda v: len(v.value_counts()))
    zeros = non_null_cnts[non_null_cnts==0].index.values
    return df.drop(zeros, axis=1)



def reverse_sigmoid(val):
    return -np.log(1 / val - 1)