import numpy as np
import pandas as pd
from sklearn.preprocessing import Imputer
from sklearn.preprocessing import StandardScaler
import helpers
import constants as c
from cleaning import separate_df_bycols


sep_df_mod = helpers.wrapped_partial(separate_df_bycols, cols=c.d_exclude)


def get_binaries(df):
    """
    returns column names where values are binary (0, 1)
    """
    binaries = df.apply(helpers.zero_comp)
    return binaries[binaries].index.values


def impute_df(df, strategy='median'):
    """
    imput dataframe np.nan values
    dummy variables get filled with 0's
    continuous variables use median imputation

    #if this is productionalized process need to return imputer
    """
    float_df = df.select_dtypes(include=['float64'])
    binary_cols = get_binaries(float_df)
    for col in binary_cols:
        df[col] = df[col].fillna(0).astype(np.int8)
    cont_cols = [col for col in float_df.columns if col not in binary_cols]
    if strategy == 'zero':
        df[cont_cols] = df[cont_cols].fillna(0)
        return df
    imp = Imputer(strategy=strategy)
    df[cont_cols] = imp.fit_transform(df[cont_cols])
    return df


def create_dummies(df):
    """
    change to categoryEncoder or w/e the name is going to be once sklearn v.20 is released
    """
    return pd.get_dummies(df)


check_nulls = lambda df: df.isnull().any(axis=0)


def standardize(df, thescaler=StandardScaler, **kwargs):
    """
    """
    binary_cols = get_binaries(df)
    to_std_cols = [col for col in df.columns if col not in binary_cols]
    scaler = thescaler(**kwargs)
    df[to_std_cols] = scaler.fit_transform(df[to_std_cols])
    return df


def val_to_bin(val, df_bins):
    if pd.isnull(val):
        row = pd.isnull(df_bins.minimum) | (df_bins.minimum == -999)
    else:
        row = (df_bins['minimum'] <= val) & (val <= df_bins['maximum'])
    assert sum(row) == 1
    return df_bins.loc[row, 'value'].iloc[0]


