select application_number
    , case when (chargeback_amount > 0 or first_inst_amount_60dpd - first_inst_amount_paid_60dpd > 1
            or  second_inst_amount_30dpd - second_inst_amount_paid_30dpd > 1) and early_payment_status is null
        then 1 else 0 end as y
from lease_summaries
where loan_application_created_date > '{bd}' 
    and first_inst_amount_60dpd > 0 and second_inst_amount_30dpd > 0 and approval_amount >= 400
;